var express = require('express');
var fs      = require('fs');
var app     = express();
 
var auth = express.basicAuth(function(user, pass, callback) {
 var result = (user === 'toto' && pass === '1234');
 callback(null /* error */, result);
});

app.get('/', function(req, res) {
  res.send('Hello world');
});

app.get('/downloads', auth, function(req, res) {
  var html = '<ul>';
  var files = getFiles('/home/twiggeek/download');
  for (var i = 0; i < files.length; i++) {
    html += '<li><a href="' + files[i] + '">' + files[i] + '</a></li>';
  };
  html += '</ul>';
  res.send(html);
});

app.get('/:file(*)', function(req, res, next){
  var file = req.params.file
    , path = __dirname + '/' + file;

  res.download(path);
});

var server = app.listen(3000, function() {
  console.log('Listening on port %d', server.address().port);
});

function getFiles(dir,files_){
  files_ = files_ || [];
  if (typeof files_ === 'undefined') files_=[];
  var files = fs.readdirSync(dir);
  for(var i in files){
      if (!files.hasOwnProperty(i)) continue;
      var name = dir+'/'+files[i];
      if (fs.statSync(name).isDirectory()){
          getFiles(name,files_);
      } else {
          files_.push(name);
      }
  }
  return files_;
}